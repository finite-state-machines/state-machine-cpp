# A ready to use state machine for C++
Copyright &copy; 2020 Frank Listing

There are a lot of variants known to implement state machines. Most of them merge the code of the state machines behavior together with the functional code. A better solution is to strictly separate the code for the state machines logic from the functional code. An existing state machine component is parametrized to define its behavior. 

This help file includes the documentation of an implemantation of a ready to use FSM (Finite State Machine). Using this state machine is very simple. Just create a state machne object and some states, after that define the transitions and the state action. eparate thread.

### Basic steps to create a State Machine
1. Model your state machine inside a graphical editor e.g. UML tool or any other applicable graphic tool.
1. Create an instance of the Fsm class in your code.
1. Create the states of the state machine.
1. Transfer all your transitions from the graphic to the code.
1. Register the action handlers for your states.
1. Start the state machine.

#### Available How to documents:
- [Implementing a State Machine with composite states.](#how-to:-create-a-state-machine-with-composite-states)
- [Using of History States.](#how-to:-use-history-states)

***

# How to: Create a State Machine with composite states
This topic shows how to implement a Finite State Machine with composite states using the StateMachine component. Additionally the main state machine is asynchronous.  
This example extends the previous example by an additional mode: Flashing the yellow light.

## Example: The steps to create a State Machine with composite states.
1. Start with the model of the state machine.  
    The model consists of three state machines in two layers.

    ![Composite state machine)](doc/Images/traffic_light_composite.png)  
    *The state machines hierarchy*

    The main machine has two composite states.  
    ![Main Machine)](doc/Images/traffic_light_main.png)  
    *The main FSM*

    The machine for the night mode (the yellow lamp is flashing). This machine will be started when the main machine changes to the "Night_Mode" state. If this machine ends the main machine changes its state to "Day_Mode".  
    ![Night Mode Machine)](doc/Images/traffic_light_night.png)  
    *The FSM for the night mode*

    The machine for the day mode. This machine will be started when the main machine changes to the "Day_Mode" state. If this machine ends the main machine changes its state to "Night_Mode".  
    ![Day Mode Machine)](doc/Images/traffic_light_day.png)  
    *The FSM for the day mode*

1. Create all necessary state machine objects.
    ```C++
    class TrafficLightController {
        //! The state machine handling the night mode of the traffic light.
        fsm::Fsm<TrafficLight&> night_controller_{"Night_Controller"};
        fsm::State<TrafficLight&> night_showing_yellow_{"Showing_Yellow"};
        fsm::State<TrafficLight&> night_showing_nothing_{"Showing_Nothing"};

        //! The state machine handling the day working mode of the traffic light.
        fsm::Fsm<TrafficLight&> day_controller_{"Day_Controller"};
        fsm::State<TrafficLight&> day_showing_red_{"Showing_Red"};
        fsm::State<TrafficLight&> day_showing_red_yellow_{"Showing_RedYellow"};
        fsm::State<TrafficLight&> day_showing_yellow_{"Showing_Yellow"};
        fsm::State<TrafficLight&> day_showing_green_{"Showing_Green"};

        //! The state machine on the top
        fsm::Fsm<TrafficLight&> main_controller_{"Main_Controller"};
        fsm::State<TrafficLight&> night_mode_{"Night_Mode"};
        fsm::State<TrafficLight&> day_mode_{"Day_Mode"};
    ...
    }
    ```
1. Define all transitions in the code using the `Transition()` method.  
Register the action handlers for all states using the `Entry()` method.  
Add the child machines to the composite states using the `Child()` method.  
To access a state use the indexer of the state machine with the name of the state. The first access to a state will create a new one.
    ```C++
    void TrafficLightController::InitializeNightController() {
        // clang-format off
        night_controller_.Initial()
            .Transition(kStartEvent, night_showing_yellow_);

        night_showing_yellow_
            .Transition(kTimerEvent, [](auto tl) { return tl.IsNightMode(); }, night_showing_nothing_)
            .Transition(kTimerEvent, Not([](auto tl) { return tl.IsNightMode(); }), night_controller_.Final())
            .Entry([](auto tl) { tl.SetLamps(false, true, false); });

        night_showing_nothing_
            .Transition(kTimerEvent, night_showing_yellow_)
            .Entry([](auto tl) { tl.SetLamps(false, false, false); });
        // clang-format on
    }
    ```
    ```C++
    void TrafficLightController::InitializeDayController() {
        // clang-format off
        day_controller_.Initial()
            .Transition(kStartEvent, day_showing_red_);
        day_showing_red_
            .Transition(kTimerEvent, Not([](auto tl) { return tl.IsNightMode(); }), day_showing_red_yellow_)
            .Transition(kTimerEvent, [](auto tl) { return tl.IsNightMode(); }, day_controller_.Final())
            .Entry([](auto tl) { tl.SetLamps(true, false, false); });
        day_showing_yellow_
            .Transition(kTimerEvent, day_showing_red_)
            .Entry([](auto tl) { tl.SetLamps(false, true, false); });
        day_showing_red_yellow_
            .Transition(kTimerEvent, day_showing_green_)
            .Entry([](auto tl) { tl.SetLamps(true, true, false); });
        day_showing_green_
            .Transition(kTimerEvent, day_showing_yellow_)
            .Entry([](auto tl) { tl.SetLamps(false, false, true); });
        // clang-format on
    }
    ```
    ```C++
    void TrafficLightController::InitializeMainController() {
        // clang-format off
        main_controller_.Initial()
            .Transition(kStartEvent, day_mode_);
        night_mode_
            .Transition(kNoEvent, day_mode_)
            .Child(night_controller_);
        day_mode_
            .Transition(kNoEvent, night_mode_)
            .Child(day_controller_);
        // clang-format on
    }
    ```
1. Start the state machine using the `Start()` method. 
    ```C++
    main_controller_.Start(traffic_light_);
    ```

#### Example (project "example")
To see this code working have a look at the 'examle' project. Use 'b' to switch between day and night mode and 'q' to quit the app. A simple \<Enter> will trigger the state machine. 

***

# How-to: Use History States
The state machine supports the use of history states. Two kinds of history states are available:
- Normal history
- Deep history

To specify history the object ```H``` (history) or ```Hx``` (deep history) has to be provided to the destination state.
![Example History)](doc/Images/example_history.png)  
*History states*

```C++
  working_
      .Transition(kBreak, handling_error_)
      .Transition(kNoEvent, finalizing_)
      .Entry([](int){std::cout << "starting Working\n";})
      .Child(fsm_working_);
  handling_error_.Transition(kRestart, working_)
      .Transition(kContinue, working_(H))
      .Transition(kContinueDeep, working_(Hx))
      .Transition(kNext, finalizing_)
      .Transition(kBreak, machine_.Final())
      .Entry([](int){std::cout << "starting HandlingError\n";})
      .Exit([](int){std::cout << "leaving HandlingError\n";});
```

#### Example (project "history_example")
To learn how history works have a look at the "history_example" project.   
Key codes:
- \<Enter> triggers a state change in normal working mode.
- 'b' will change from any sub state of "Working" to "Handling Error".
- 'r' changes from "Handling Error" to "Working" without any history.
- 'c' changes from "Handling Error" to "Working" using normal history.
- 'd' changes from "Handling Error" to "Working" using deep history.

***
Copyright &copy; 2020 Frank Listing
