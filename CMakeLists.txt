cmake_minimum_required(VERSION 3.10)

project(fsm_cpp VERSION 1.0)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED True)

include_directories(state_machine)

add_subdirectory(example)
add_subdirectory(history_example)
