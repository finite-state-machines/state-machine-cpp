/*
  Copyright (c) 20016-2020 Frank Listing

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#ifndef FSM_STATE_H
#define FSM_STATE_H

#include <cassert>
#include <vector>

#include "data.h"
#include "event.h"

namespace fsm {

template <typename T>
class Fsm;

/**
 * This class represents a particular state of the state machine.
 * @tparam T The type of data provided to the condition and action handlers.
 */
template <typename T>
class State {
 protected:
  /**
   * Possible state types.
   */
  enum class StateType {
    kNormal,
    kInitial,
    kFinal,
  };

 private:
  //! The name of the state.
  const char* name_;

  //! The type of the state.
  const StateType type_{StateType::kNormal};

  //! A container to store the transition information.
  std::vector<TransitionData<T>> transitions_;

  //! A container to store the child machines.
  std::vector<FsmData<Fsm<T>>> children_{};

  //! The handler method for the states entry action.
  std::function<void(T)> on_entry_;

  //! The handler method for the states exit action.
  std::function<void(T)> on_exit_;

  //! The handler method for the reaction in the state.
  std::function<void(T)> on_do_;

 public:
  /**
   * Initializes a new instance of the @see State class.
   * @param name The name of the state (will store the pointer only).
   */
  explicit State(const char* name) : name_{name} {}

  //! Deleted special member function to prevent of copying and moving.
  State(const State&) = delete;

  //! Deleted special member function to prevent of copying and moving.
  State(State&&) = delete;

  //! Deleted special member function to prevent of copying and moving.
  State& operator=(const State&) = delete;

  //! Deleted special member function to prevent of copying and moving.
  State& operator=(State&&) = delete;

  /**
   * Use default destructor.
   */
  ~State() = default;

  /**
   * Gets the name of the state.
   */
  const char* Name() const { return name_; }

  /**
   * Gets a value indicating whether this instance is the initial state.
   */
  bool IsInitial() const { return type_ == StateType::kInitial; }

  /**
   * Gets a value indicating whether this instance is the final state.
   */
  bool IsFinal() const { return type_ == StateType::kFinal; }

  /**
   * Calls the OnEntry handler of this state and starts all child FSMs if there
   * are some.
   * @param data The data object.
   * @param history A value, containing the information which history model has
   * to be used.
   */
  void Start(T& data, const History history) const {
    if (history.IsHistory() && TryStartHistory(data)) {
      return;
    }

    if (history.IsDeepHistory() && TryStartDeepHistory(data)) {
      return;
    }

    FireOnEntry(data);
    StartChildren(data);
  }

  /**
   * To resume the work of a state e.g. after a reboot.
   * @remarks Does not call the entry function, but starts all child FSMs if there are some.
   * @param data The data object.
   */
  void Resume(T& data) const { StartChildren(data); }

  /**
   * Let all direct child FSMs continue working. Returns false if there is no active child.
   * If this method returns false, @see Start has to be called to complete the operation.
   * @param data The data object.
   * @return Returns true if there are active children; false otherwise.
   */
  bool TryStartHistory(T& data) const {
    if (!HasActiveChildren()) {
      return false;
    }

    for (auto& child : children_) {
      child.Fsm().GetCurrentState().Start(data, History{});
    }

    return true;
  }

  /**
   * Let all child FSMs continue working. Returns false if there is no active child.
   * If this method returns false, @see Start has to be called to complete the operation.
   * @param data The data object.
   * @return Returns true if there are active children; false otherwise.
   */
  bool TryStartDeepHistory(T& data) const { return HasActiveChildren(); }

  /**
   * Adds a list with child machines.
   * @param child The child machine to add.
   * @returns Returns a reference to the state object to support method
   * chaining.
   */
  State<T>& Child(Fsm<T>& child) {
    if (!IsInitial() && !IsFinal()) {
      children_.emplace_back(child);
    }

    return *this;
  }

  /**
   * Sets the handler method for the states entry action.
   * @param action The handler method for the states entry action.
   * @returns Returns a reference to the state object to support method
   * chaining.
   */
  State<T>& Entry(const std::function<void(T)>& action) {
    if (!IsInitial() && !IsFinal()) {
      on_entry_ = action;
    }

    return *this;
  }

  /**
   * Sets the handler method for the states exit action.
   * @param action The handler method for the states exit action.
   * @returns Returns a reference to the state object to support method
   * chaining.
   */
  State<T>& Exit(const std::function<void(T)>& action) {
    if (!IsInitial() && !IsFinal()) {
      on_exit_ = action;
    }

    return *this;
  }

  /**
   * Sets the handler method for the reaction in the state.
   * @param action The handler method for the states do action.
   * @returns Returns a reference to the state object to support method
   * chaining.
   */
  State<T>& Do(const std::function<void(T)>& action) {
    if (!IsInitial() && !IsFinal()) {
      on_do_ = action;
    }

    return *this;
  }

  /**
   * Creates an endpoint object from this state and the given history
   * information.
   * @param history An object containing the history value.
   */
  TransitionEndPoint<T> operator()(const History& history) { return TransitionEndPoint<T>(*this, history); }

  /**
   * Adds a new transition to the state.
   * @param trigger The Event that initiates this transition.
   * @param endpoint A reference to the end point of this transition (including
   * the information about history).
   */
  State<T>& Transition(const FsmEvent& trigger, TransitionEndPoint<T> endpoint) {
    return Transition(trigger, nullptr, endpoint);
  }

  /**
   * Adds a new transition to the state.
   * @param trigger The Event that initiates this transition.
   * @param condition Condition handler of this transition.
   * @param endpoint A reference to the end point of this transition (including
   * the information about history).
   */
  State<T>& Transition(const FsmEvent& trigger, std::function<bool(T)> condition, TransitionEndPoint<T> endpoint) {
    HandleStartState(trigger, condition);
    transitions_.emplace_back(trigger, condition, endpoint.to, endpoint.history);
    return *this;
  }

  /**
   * Triggers a transition.
   * @param trigger The event occurred.
   * @param data The data provided to the condition and action handlers.
   * @returns Returns data indicating whether the event was handled and needed
   * to proceed with the new state.
   */
  ChangeStateData<T> Trigger(const FsmEvent& trigger, T& data) const {
    const auto* effective_trigger = &trigger;
    if (ProcessChildren(&effective_trigger, data)) {
      return ChangeStateData<T>{true};
    }

    return ProcessTransitions(*effective_trigger, data);
  }

  /**
   * Triggers the reaction in the state.
   * @param data The data provided to the condition and action handlers.
   */
  void Do(T& data) const {
    DoChildren(data);
    FireDo(data);
  }

  /**
   * Prints the current state name and, if avaialble, the names of all sub states.
   * @param level The level to provide to the output callback.
   * @param state_callback A callback function which will be called for every state. The first parameter is the level in
   * the hierarchy of the state machines (the same level can be occur more than once if there are concurrent sub
   * states).
   */
  void Dump(int level, const std::function<void(int, const char*)>& stateCallback) const {
    if (!stateCallback) {
      return;
    }

    stateCallback(level, name_);

    for (const auto& child : children_) {
      child.Fsm().GetCurrentState().Dump(level + 1, stateCallback);
    }
  }

 protected:
  /**
   * Initializes a new instance of the @see State class.
   * This constructor is only for special state usage.
   * @param name The name of the state.
   * @param type The type of the state.
   */
  State(const char* name, const StateType type) : name_{name}, type_{type} {}

 private:
  /**
   * Gets a value indicating whether this state has active child machines.
   */
  bool HasActiveChildren() const {
    for (auto& child : children_) {
      if (!child.Fsm().HasFinished()) {
        return true;
      }
    }

    return false;
  }

  /**
   * Starts all registered sub state machines.
   * @param data The data object.
   */
  void StartChildren(T& data) const {
    for (auto& child : children_) {
      child.Fsm().Start(data);
    }
  }

  /**
   * Handles the start state.
   * If this is the start state and the transition does not fit the special
   * requirements, an exception will be thrown.
   * @param trigger The Event that initiates this transition.
   * @param condition Condition handler of this transition.
   * @return Returns true if the parameters are correct; false otherwise
   */
  bool HandleStartState(const FsmEvent& trigger, std::function<bool(T)> condition) {
    if (!IsInitial()) {
      return true;
    }

    // The start state must have exactly one transition
    // with the start event (fsm::kStartEvent) and without a condition!
    if (!transitions_.empty() || condition != nullptr || trigger != kStartEvent) {
      assert(false);
      return false;
    }

    return true;
  }

  /**
   * Processes the transitions.
   * @param trigger The event occurred.
   * @param data The data provided to the condition and action handlers.
   * @returns Returns data indicating whether the event was handled and needed
   * to proceed with the new state.
   */
  ChangeStateData<T> ProcessTransitions(const FsmEvent& trigger, T& data) const {
    for (auto transition : transitions_) {
      if (transition.TriggerMatch(trigger) && transition.Condition(data)) {
        return ChangeState(transition, data);
      }
    }

    return ChangeStateData<T>{false};
  }

  /**
   * Processes the child machines.
   * @remarks If the last child machine went to the final state, the parameter
   *          "trigger" is changed to @see FsmEvent::NoEvent and the method
   *          returns false.
   * @param trigger The event occurred.
   * @param data The data provided to the condition and action handlers.
   * @returns Returns true if the event was handled, false otherwise.
   */
  bool ProcessChildren(const FsmEvent** trigger, T& data) const {
    if (!HasActiveChildren()) {
      return false;
    }

    auto handled = TriggerChildren(**trigger, data);
    if (handled) {
      return true;
    }

    if (!HasActiveChildren()) {
      *trigger = &kNoEvent;
    }

    return false;
  }

  /**
   * Executes the Do action on the child machines.
   * @param data The data provided to the condition and action handlers.
   */
  void DoChildren(T& data) const {
    for (auto& child : children_) {
      if (child.Fsm().HasFinished()) {
        continue;
      }

      child.Fsm().Do(data);
    }
  }

  /**
   * Changes the state to the one stored in the transition object.
   * @param transition The actual transition.
   * @param data The data provided to the condition and action handlers.
   * @returns Returns the data needed to proceed with the new state.
   */
  ChangeStateData<T> ChangeState(TransitionData<T>& transition, T& data) const {
    FireOnExit(data);

    ChangeStateData<T> result{!transition.To().IsFinal(), &transition.To(), transition.History()};
    return result;
  }

  /**
   * Triggers the child machines.
   * @param trigger The event occurred.
   * @param data The data provided to the condition and action handlers.
   * @returns Returns true if the event was handled, false otherwise.
   */
  bool TriggerChildren(const FsmEvent& trigger, T& data) const {
    auto handled = false;
    for (auto& child : children_) {
      if (child.Fsm().HasFinished()) {
        continue;
      }

      handled |= child.Fsm().Trigger(trigger, data);
    }

    return handled;
  }

  /**
   * Fires the OnEntry event.
   * @param data The data object.
   */
  void FireOnEntry(T& data) const { Fire(on_entry_, data); }

  /**
   * Fires the OnExit event.
   * @param data The data object.
   */
  void FireOnExit(T& data) const { Fire(on_exit_, data); }

  /**
   * Fires the Do event.
   * @param data The data object.
   */
  void FireDo(T& data) const { Fire(on_do_, data); }

  /**
   * Calls one of the actions of the state
   * @param handler Action to perform.
   * @param data The data object.
   */
  static void Fire(const std::function<void(T)>& handler, T& data) {
    if (handler == nullptr) {
      return;
    }

    handler(data);
  }
};

/**
 * A class to model the special state 'initial'.
 */
template <typename T>
class InitialState final : public State<T> {
 public:
  /**
   * Initializes a new instance of the @see InitialState class.
   */
  InitialState() : State<T>("Initial", State<T>::StateType::kInitial) {}
};

/**
 * A class to model the special state 'final'.
 */
template <typename T>
class FinalState final : public State<T> {
 public:
  /**
   * Initializes a new instance of the @see FinalState class.
   */
  FinalState() : State<T>("Final", State<T>::StateType::kFinal) {}
};

}  // namespace fsm

#endif  // FSM_STATE_H
