/*
  Copyright (c) 20016-2020 Frank Listing

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#ifndef FSM_EVENT_H
#define FSM_EVENT_H

#include <cstring>

namespace fsm {

/**
 * A class representing an event.
 */
class FsmEvent {
  //! Member to store the internal ID.
  const char* id_{};

 public:
  /**
   * Initializes a new instance of the FsmEvent class.
   * @remarks Creates an unique ID to identify the event.
   */
  explicit constexpr FsmEvent(const char* id) : id_{id} {}

  /**
   * Default destructor.
   */
  ~FsmEvent() = default;

  /**
   * Deleted special member function to prevent of copying and moving.
   */
  FsmEvent(const FsmEvent&) = delete;

  /**
   * Deleted special member function to prevent of copying and moving.
   */
  FsmEvent(FsmEvent&&) = delete;

  /**
   * Deleted special member function to prevent of copying and moving.
   */
  FsmEvent& operator=(const FsmEvent&) = delete;

  /**
   * Deleted special member function to prevent of copying and moving.
   */
  FsmEvent& operator=(FsmEvent&&) = delete;

  /**
   * Gets the ID.
   */
  const char* GetId() const { return id_; }
};

/**
 * Compares the contents of two event objects.
 * @param lhs the fist object to compare.
 * @param rhs the second object to compare.
 * @return Returns true if the contents of the containers are equal, false
 * otherwise.
 */
inline bool operator==(const FsmEvent& lhs, const FsmEvent& rhs) { return strcmp(lhs.GetId(), rhs.GetId()) == 0; }

/**
 * Compares the contents of two event objects.
 * @param lhs the fist object to compare.
 * @param rhs the second object to compare.
 * @return Returns true if the contents of the containers are equal, false
 * otherwise.
 */
inline bool operator!=(const FsmEvent& lhs, const FsmEvent& rhs) { return !(lhs == rhs); }

/**
 * The Event used to start the behavior of a FSM.
 */
constexpr FsmEvent kStartEvent{"Start"};

/**
 * Gets the constant used to specify that no event is necessary.
 */
constexpr FsmEvent kNoEvent{"No"};

}  // namespace fsm

#endif  // FSM_EVENT_H
