/*
  Copyright (c) 20016-2020 Frank Listing

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#ifndef FSM_FSM_H
#define FSM_FSM_H

#include "state.h"

namespace fsm {

/**
 * Class managing the states of a synchronous FSM (finite state machine).
 * @tparam T The type of data provided to the condition and action handlers.
 */
template <typename T>
class Fsm {
  //! The name of the state machine.
  const char* name_;

  //! The initial state.
  InitialState<T> initial_{};

  //! Holds the actual state of the state machine.
  const State<T>* current_state_;

#if defined(_DEBUG) || defined(DEBUG)
  //! A handler function to handle a state change.
  std::function<void(const State<T>*, const State<T>*, const T&)> state_changed_handler_;
#endif

 public:
  /**
   * Initializes a new instance of the Fsm class.
   * @param name The name of the FSM.
   */
  explicit Fsm(const char* name) : name_{name}, current_state_{&initial_} {}

  /**
   * Gets the currently active state.
   */
  const State<T>& GetCurrentState() const { return *current_state_; }

  /**
   * Gets the initial state.
   */
  State<T>& Initial() { return initial_; }

  /**
   * Gets the final state.
   */
  State<T>& Final() {
    static FinalState<T> final{};  // NOLINT(clang-diagnostic-exit-time-destructors)
    return final;
  }

  /**
   * Gets a value indicating whether the automaton has started.
   */
  bool IsRunning() const { return current_state_ != nullptr && !current_state_->IsInitial() && !HasFinished(); }

  /**
   * Gets a value indicating whether the automaton has reached the final state.
   */
  bool HasFinished() const { return current_state_ != nullptr && current_state_->IsFinal(); }

  /**
   * Gets the name of the state machine.
   */
  const char* Name() const { return name_; }

  /**
   * Starts the behavior of the Fsm class. Executes the transition from the
   * start state to the first user defined state.
   * @remarks This method calls the initial states OnEntry method.
   * @param data The data object.
   */
  void Start(T data) {
    current_state_ = &initial_;
    Trigger(kStartEvent, data);
  }

  /**
   * Sets the provided state as active state and  starts child machines if present, e.g. to resume after a reboot.
   * @remarks This method does not call the entry function of the state.
   * @param state The state to set as current state.
   * @param data The data object.
   */
  void Resume(State<T>* state, T data) {
    if (state == nullptr) {
      return;
    }

    current_state_ = state;
    current_state_->Resume(data);
  }

  /**
   * This method is not intended for normal use. Sets the provided state as active state.
   * @remarks Use this method for testing purposes or to recover from a reboot.
   * @param state The state to set as current state.
   */
  void SetState(State<T>* state) {
    if (state == nullptr) {
      return;
    }

    current_state_ = state;
  }

  /**
   * Triggers a transition.
   * @param trigger The event occurred.
   * @param data The data provided to the condition and action handlers.
   * @returns Returns true if the event was handled, false otherwise.
   */
  bool Trigger(const FsmEvent& trigger, T data) {
    if (trigger == kNoEvent || current_state_ == nullptr) {
      return false;
    }

    auto handled_data = current_state_->Trigger(trigger, data);
    ActivateState(handled_data, data);

    return handled_data.handled;
  }

  /**
   * Triggers the reaction in the state.
   * @param data The data provided to the condition and action handlers.
   */
  void Do(T data) const {
    if (current_state_ == nullptr) {
      return;
    }

    current_state_->Do(data);
  }

  /**
   * Prints the current state name and, if avaialble, the names of all sub states.
   * @param state_callback A callback function which will be called for every state. The first parameter is the level in
   * the hierarchy of the state machines (the same level can be occur more than once if there are concurrent sub
   * states).
   */
  void Dump(std::function<void(int, const char*)> state_callback) const {
    if (current_state_ == nullptr) {
      return;
    }

    current_state_->Dump(0, state_callback);
  }

#if defined(_DEBUG) || defined(DEBUG)
  /**
   * Sets the state changed handler.
   * @param state_changed_handler The state changed handler.
   */
  void SetStateChangedHandler(std::function<void(const State<T>*, const State<T>*, const T&)> state_changed_handler) {
    state_changed_handler_ = state_changed_handler;
  }
#else
  /**
   * Empty function, does nothing.
   * @param state_changed_handler The state changed handler - ignored.
   */
  void SetStateChangedHandler(std::function<void(const State<T>*, const State<T>*, const T&)> state_changed_handler) {}
#endif

 private:
  /**
   * Activates the new state.
   * @param change_state_data The data needed to activate the new state.
   * @param data The data.
   */
  void ActivateState(ChangeStateData<T> change_state_data, T& data) {
    if (change_state_data.state_to == nullptr) {
      return;
    }

#if defined(_DEBUG) || defined(DEBUG)
    auto old_state = current_state_;
#endif
    current_state_ = change_state_data.state_to;

#if defined(_DEBUG) || defined(DEBUG)
    RaiseStateChanged(old_state, current_state_, data);
#endif

    current_state_->Start(data, change_state_data.history);
  }

#if defined(_DEBUG) || defined(DEBUG)
  /**
   * Raises the state changed event.
   * @param state_from The state before the state change.
   * @param state_to The state after the state change.
   * @param data a reference to the data of the transition.
   */
  void RaiseStateChanged(const State<T>* state_from, const State<T>* state_to, const T& data) {
    if (state_changed_handler_ == nullptr) {
      return;
    }

    state_changed_handler_(state_from, state_to, data);
  }
#endif
};

}  // namespace fsm

#endif  // FSM_FSM_H
