/*
  Copyright (c) 20016-2020 Frank Listing

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#ifndef FSM_DATA_H
#define FSM_DATA_H

#include <functional>

#include "event.h"

namespace fsm {

template <typename T>
class State;

/**
 * Helper class to have a well defined type to support history
 */
class History final {
  /**
   * The possible history types.
   */
  enum class HistoryType : unsigned char {
    kNone,
    kHistory,
    kDeepHistory,
  };

  //! The type of history represented by this object.
  const HistoryType history_;

 public:
  /**
   * Initializes a new instance of the History class (not using history).
   */
  constexpr History() : History{HistoryType::kNone} {}

  /**
   * Gets an instance representing history.
   */
  constexpr static History GetHistory() { return History(HistoryType::kHistory); }

  /**
   * Gets an instance representing deep history.
   */
  constexpr static History GetDeepHistory() { return History(HistoryType::kDeepHistory); }

  /**
   * Gets a value indicating whether to use history.
   */
  constexpr bool IsHistory() const { return history_ == HistoryType::kHistory; }

  /**
   * Gets a value indicating whether to use deep history.
   */
  constexpr bool IsDeepHistory() const { return history_ == HistoryType::kDeepHistory; }

 private:
  /**
   * Initializes a new instance of the History class.
   * @param history The history value to store.
   */
  constexpr explicit History(const HistoryType history) : history_{history} {}
};

// ReSharper disable once CppInconsistentNaming
//! A constant holding the information for history.
constexpr History H = History::GetHistory();

// ReSharper disable once CppInconsistentNaming
//! A constant holding the information for deep history.
constexpr History Hx = History::GetDeepHistory();

/**
 * A functor used to create a condition with a reversed result.
 */
template <typename TFunc>
class NotHelper final {
  //! Stores the function pointer from which the result is negated.
  TFunc func_;

 public:
  /**
   * Initializes a new instance of the NotHelper class.
   * @param func A function pointer from which the result is negated.
   */
  explicit NotHelper(const TFunc& func) : func_{func} {}

  /**
   * Calls the store function pointer and negates it's result.
   */
  template <typename TParam>
  bool operator()(TParam&& param) {
    return !func_(std::forward<TParam>(param));
  }
};

/**
 * A function to create a condition with a reversed result.
 * @param condition A condition to check negated for a transition.
 */
template <typename T>
NotHelper<T> Not(const T condition) {
  return NotHelper<T>{condition};
}

/**
 * A class to store all information about the destination of a transition.
 */
template <typename Ty>
struct TransitionEndPoint final {
  //! The destination state of the transition.
  State<Ty>& to;

  //! The type of history to use, the default is none.
  const History history{};

  // ReSharper disable once CppNonExplicitConvertingConstructor
  /**
   * Initializes a new instance of the TransitionEndPoint class.
   * @param to The destination state of the transition.
   */
  TransitionEndPoint(State<Ty>& to) : TransitionEndPoint{to, History{}} {}

  /**
   * Initializes a new instance of the TransitionEndPoint class.
   * @param to The destination state of the transition.
   * @param history The type of history to use.
   */
  TransitionEndPoint(State<Ty>& to, const History history) : to{to}, history{history} {}
};

/**
 * Class holding all information about a transition.
 * @tparam T The type of data provided to the condition and action handlers.
 */
template <typename T>
class TransitionData {
  //! The Event that initiates this transition.
  const FsmEvent& trigger_;

  //! The condition handler of this transition.
  std::function<bool(T)> condition_;

  //! A reference to the end point of this transition.
  State<T>& to_;

  //! A value containing the information whether to use history.
  fsm::History history_{};

 public:
  /**
   * Initializes a new instance of the Transition class.
   * @param trigger The Event that initiates this transition.
   * @param condition Condition handler of this transition.
   * @param to A reference to the end point of this transition.
   * @param history A value containing the information whether to use history.
   */
  TransitionData(const FsmEvent& trigger, std::function<bool(T)> condition, State<T>& to, const History history)
      : trigger_{trigger}, condition_{std::move(condition)}, to_{to}, history_{history} {}

  /**
   * Gets a value indicating whether the stored trigger is equal to the trigger
   * given as parameter.
   * @param trigger The value to compare with the stored trigger.
   * @return Returns true is the values are equal; false otherwise.
   */
  bool TriggerMatch(const FsmEvent& trigger) const { return trigger_ == trigger; }

  /**
   * Executes the condition handler of this transition.
   * @param data The data provided to the condition and action handlers.
   * @returns If there is no condition handler -> returns true. If there is a
   * condition handler, take it's return value.
   */
  bool Condition(T& data) { return condition_ == nullptr || condition_(data); }

  /**
   * Get the history information.
   */
  const fsm::History& History() const { return history_; }

  /**
   * Gets a reference to the end point of this transition.
   */
  const State<T>& To() { return to_; }
};

/**
 * A class needed to store references of a state machine into an array.
 */
template <typename TFsm>
class FsmData {
  //! A reference to the stored state machine.
  TFsm& fsm_;

 public:
  /**
   * Initializes a new instance of the @see FsmData class.
   * @param fsm A reference to the state machine to store.
   */
  // ReSharper disable once CppNonExplicitConvertingConstructor
  constexpr FsmData(TFsm& fsm) : fsm_{fsm} {}

  /**
   * Gets a reference to the stored state machine.
   */
  TFsm& Fsm() const { return fsm_; }
};

/**
 * A class to store data regarding to a state change.
 */
template <typename T>
struct ChangeStateData {
  //! A value indicating whether an event was handled.
  const bool handled{};

  //! A pointer to the state to change to.
  const State<T>* state_to{};

  //! The information whether to switch to a history state.
  const History history{};

  /**
   * Initializes a new instance of the @see ChangeStateData class.
   * @param handled A value indicating whether an event was handled.
   * @param state_to A pointer to the state to change to.
   * @param history The information whether to switch to a history state.
   */
  ChangeStateData(const bool handled, const State<T>* state_to, const History history)
      : handled{handled}, state_to{state_to}, history{history} {}

  /**
   * Initializes a new instance of the @see ChangeStateData class.
   * @param handled A value indicating whether an event was handled.
   */
  explicit ChangeStateData(const bool handled) : handled{handled} {}
};
}  // namespace fsm

#endif  // FSM_DATA_H
