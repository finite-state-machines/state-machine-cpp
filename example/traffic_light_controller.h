#ifndef TRAFFIC_LIGHT_CONTROLLER_H
#define TRAFFIC_LIGHT_CONTROLLER_H

#include "fsm.h"
#include "traffic_light.h"

/**
 * The event used to trigger the state machines.
 */
constexpr fsm::FsmEvent kTimerEvent{"Tick"};

/**
 * A class controlling the traffic light.
 */
class TrafficLightController {
  //! The state machine handling the night mode of the traffic light.
  fsm::Fsm<TrafficLight&> night_controller_{"Night_Controller"};
  fsm::State<TrafficLight&> night_showing_yellow_{"Showing_Yellow"};
  fsm::State<TrafficLight&> night_showing_nothing_{"Showing_Nothing"};

  //! The state machine handling the day working mode of the traffic light.
  fsm::Fsm<TrafficLight&> day_controller_{"Day_Controller"};
  fsm::State<TrafficLight&> day_showing_red_{"Showing_Red"};
  fsm::State<TrafficLight&> day_showing_red_yellow_{"Showing_RedYellow"};
  fsm::State<TrafficLight&> day_showing_yellow_{"Showing_Yellow"};
  fsm::State<TrafficLight&> day_showing_green_{"Showing_Green"};

  //! The state machine on the top
  fsm::Fsm<TrafficLight&> main_controller_{"Main_Controller"};
  fsm::State<TrafficLight&> night_mode_{"Night_Mode"};
  fsm::State<TrafficLight&> day_mode_{"Day_Mode"};

  //! The traffic light object to control
  TrafficLight traffic_light_;

 public:
  /**
   * Initializes a new instance of the TrafficLight class.
   */
  TrafficLightController();

  /**
   * Sends the timer event to the main controller.
   */
  void SendTimerEvent();

  /**
   * Toggles between day mode and night mode.
   */
  void SwitchMode();

 private:
  void InitializeNightController();
  void InitializeDayController();
  void InitializeMainController();
  void StartBehavior();
};

#endif  // TRAFFIC_LIGHT_CONTROLLER_H
