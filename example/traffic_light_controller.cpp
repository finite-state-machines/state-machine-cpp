#include "traffic_light_controller.h"

using namespace fsm;

/**
 * Initializes a new instance of the TrafficLight class.
 */
TrafficLightController::TrafficLightController() {
  InitializeNightController();
  InitializeDayController();
  InitializeMainController();
  StartBehavior();
}

/**
 * Sends the timer event to the main controller.
 */
void TrafficLightController::SendTimerEvent() { main_controller_.Trigger(kTimerEvent, traffic_light_); }

/**
 * Toggles between day mode and night mode.
 */
void TrafficLightController::SwitchMode() { traffic_light_.SetNightMode(!traffic_light_.IsNightMode()); }

void TrafficLightController::InitializeNightController() {
  // clang-format off
  night_controller_.Initial()
      .Transition(kStartEvent, night_showing_yellow_);

  night_showing_yellow_
      .Transition(kTimerEvent, [](auto tl) { return tl.IsNightMode(); }, night_showing_nothing_)
      .Transition(kTimerEvent, Not([](auto tl) { return tl.IsNightMode(); }), night_controller_.Final())
      .Entry([](auto tl) { tl.SetLamps(false, true, false); });

  night_showing_nothing_
      .Transition(kTimerEvent, night_showing_yellow_)
      .Entry([](auto tl) { tl.SetLamps(false, false, false); });
  // clang-format on
}

void TrafficLightController::InitializeDayController() {
  // clang-format off
  day_controller_.Initial()
      .Transition(kStartEvent, day_showing_red_);
  day_showing_red_
      .Transition(kTimerEvent, Not([](auto tl) { return tl.IsNightMode(); }), day_showing_red_yellow_)
      .Transition(kTimerEvent, [](auto tl) { return tl.IsNightMode(); }, day_controller_.Final())
      .Entry([](auto tl) { tl.SetLamps(true, false, false); });
  day_showing_yellow_
      .Transition(kTimerEvent, day_showing_red_)
      .Entry([](auto tl) { tl.SetLamps(false, true, false); });
  day_showing_red_yellow_
      .Transition(kTimerEvent, day_showing_green_)
      .Entry([](auto tl) { tl.SetLamps(true, true, false); });
  day_showing_green_
      .Transition(kTimerEvent, day_showing_yellow_)
      .Entry([](auto tl) { tl.SetLamps(false, false, true); });
  // clang-format on
}

void TrafficLightController::InitializeMainController() {
  // clang-format off
  main_controller_.Initial()
      .Transition(kStartEvent, day_mode_);
  night_mode_
      .Transition(kNoEvent, day_mode_)
      .Child(night_controller_);
  day_mode_
      .Transition(kNoEvent, night_mode_)
      .Child(day_controller_);
  // clang-format on
}

void TrafficLightController::StartBehavior() { main_controller_.Start(traffic_light_); }
