#include <iostream>
#include <string>

#include "traffic_light_controller.h"

void ProcessInput(const char input, TrafficLightController& traffic_light) {
  if (input == 'b' || input == 'B') {
    traffic_light.SwitchMode();
    return;
  }

  traffic_light.SendTimerEvent();
}

char GetSingleCharacter() {
  std::string line;
  std::getline(std::cin, line);
  return line.empty() ? 't' : line[0];
}

void RunTrafficLight() {
  try {
    TrafficLightController traffic_light;

    while (true) {
      const auto input = GetSingleCharacter();
      if (input == 'q' || input == 'Q') {
        break;
      }

      ProcessInput(input, traffic_light);
    }
  } catch (...) {
  }
}

int main() { RunTrafficLight(); }
