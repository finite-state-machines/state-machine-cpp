#ifndef LAMP_H
#define LAMP_H

/**
 * A class representing a lamp.
 */
class Lamp {
  //! A value indicating whether the lamp is switched on.
  bool is_on_{};

 public:
  /**
   * Gets a value indicating whether the lamp is switched on.
   * @return Returns true if the lamp is switched on; false otherwise.
   */
  bool IsOn() const { return is_on_; }

  /**
   * Sets a value indicating whether the lamp is switched on.
   * @return Returns true if the lamp is switched on; false otherwise.
   */
  void IsOn(const bool value) { is_on_ = value; }

  /**
   * Gets a 'x' if the lamp is switched on, a '-' otherwise.
   * @return Returns true if the lamp is switched on; false otherwise.
   */
  char GetState() const { return is_on_ ? 'X' : '-'; }
};

#endif  // LAMP_H
