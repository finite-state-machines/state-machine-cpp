#include "traffic_light.h"

#include <iostream>

/**
 * Sets the check marks for the lamps of the traffic light.
 * @param red True to check the red lamp, false otherwise.
 * @param yellow True to check the yellow lamp, false otherwise.
 * @param green True to check the green lamp, false otherwise.
 */
void TrafficLight::SetLamps(const bool red, const bool yellow, const bool green) {
  red_.IsOn(red);
  yellow_.IsOn(yellow);
  green_.IsOn(green);

  std::cout << red_.GetState() << " " << yellow_.GetState() << " " << green_.GetState() << "\n";
}
