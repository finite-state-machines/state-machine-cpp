#ifndef TRAFFIC_LIGHT_H
#define TRAFFIC_LIGHT_H

#include "lamp.h"

/**
 * A class representing a traffic light.
 */
class TrafficLight {
  //! The red lamp.
  Lamp red_{};

  //! The yellow lamp.
  Lamp yellow_{};

  //! The green lamp.
  Lamp green_{};

  //! A value indicating whether system is in night mode.
  bool is_night_mode_{};

 public:
  /**
   * Gets the red lamp.
   */
  Lamp& Red() { return red_; }

  /**
   * Gets the yellow lamp.
   */
  Lamp& Yellow() { return yellow_; }

  /**
   * Gets the green lamp.
   */
  Lamp& Green() { return green_; }

  /**
   * Gets a value indicating whether system is in night mode.
   */
  bool IsNightMode() const { return is_night_mode_; }

  /**
   * Sets a value indicating whether system is in night mode.
   */
  void SetNightMode(const bool value) { is_night_mode_ = value; }

  /**
   * Sets the check marks for the lamps of the traffic light.
   * @param red True to check the red lamp, false otherwise.
   * @param yellow True to check the yellow lamp, false otherwise.
   * @param green True to check the green lamp, false otherwise.
   */
  void SetLamps(bool red, bool yellow, bool green);
};

#endif  // TRAFFIC_LIGHT_H
