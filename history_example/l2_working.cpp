#include "l2_working.h"

#include <iostream>

#include "events.h"

using namespace fsm;

L2Working::L2Working() {
  // clang-format off
  machine_.Initial()
      .Transition(kStartEvent, l3_w1_);
  l3_w1_
      .Transition(kNext, l3_w2_)
      .Entry([](int){std::cout << "starting W1\n";});
  l3_w2_
      .Transition(kNext, l3_w3_)
      .Entry([](int){std::cout << "starting W2\n";});
  l3_w3_
      .Transition(kNext, l3_w4_)
      .Entry([](int){std::cout << "starting W3\n";})
      .Do([](int){std::cout << "doing W3\n";});
  l3_w4_
      .Transition(kNext, l3_w5_)
      .Entry([](int){std::cout << "starting W4\n";});
  l3_w5_
      .Transition(kNext, machine_.Final())
      .Entry([](int){std::cout << "starting W5\n";});
  // clang-format on
}
