#ifndef MAIN_MACHINE_H
#define MAIN_MACHINE_H

#include "working.h"

class MainMachine {
  Working fsm_working_;

  fsm::Fsm<int> machine_{"Main"};

  fsm::State<int> initializing_{"Initializing"};
  fsm::State<int> working_{"Working"};
  fsm::State<int> handling_error_{"HandlingError"};
  fsm::State<int> finalizing_{"Finalizing"};

 public:
  MainMachine();
  fsm::Fsm<int>& GetMachine() { return machine_; }

  void Start();
  void DumpStates();
};

#endif  // MAIN_MACHINE_H
