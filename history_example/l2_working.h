#ifndef L2_WORKING_H
#define L2_WORKING_H

#include "fsm.h"

class L2Working {
  fsm::Fsm<int> machine_{"L2-Working"};

  fsm::State<int> l3_w1_{"L3-W1"};
  fsm::State<int> l3_w2_{"L3-W2"};
  fsm::State<int> l3_w3_{"L3-W3"};
  fsm::State<int> l3_w4_{"L3-W4"};
  fsm::State<int> l3_w5_{"L3-W5"};

 public:
  L2Working();

  constexpr operator fsm::Fsm<int> &() { return machine_; }
};

#endif  // L2_WORKING_H
