#include <iostream>
#include <map>
#include <string>

#include "events.h"
#include "main_machine.h"

using namespace fsm;

MainMachine main_machine;

const std::map<char, const FsmEvent&> kEvents{
    {'B', kBreak}, {'C', kContinue}, {'D', kContinueDeep}, {'N', kNext}, {'R', kRestart},
    {'b', kBreak}, {'c', kContinue}, {'d', kContinueDeep}, {'n', kNext}, {'r', kRestart},
};

void Process(const char key) {
  const auto it = kEvents.find(key);
  if (it != kEvents.end()) {
    main_machine.GetMachine().Trigger(it->second, 42);
    main_machine.DumpStates();
  }

  if (key == '#') {
    main_machine.GetMachine().Do(23);
  }
}

void OnStateChanged(const State<int>* state_from, const State<int>* state_to, const int& data) {
  std::cout << "State change: " << state_from->Name() << " --> " << state_to->Name() << "\n";
}

char GetSingleCharacter() {
  std::string line;
  std::getline(std::cin, line);
  return line.empty() ? 'n' : line[0];
}

void Run() {
  try {
    main_machine.GetMachine().SetStateChangedHandler(OnStateChanged);

    main_machine.Start();

    while (true) {
      const auto input = GetSingleCharacter();
      if (input == 'q' || input == 'Q') {
        break;
      }

      Process(input);
    }

    std::cout << "The end!\n";
  } catch (...) {
  }
}

int main() noexcept { Run(); }
