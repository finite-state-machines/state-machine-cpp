#include "main_machine.h"

#include <iostream>

#include "events.h"

using namespace fsm;

MainMachine::MainMachine() {
  // clang-format off
  machine_.Initial()
      .Transition(kStartEvent, initializing_);
  initializing_
      .Transition(kNext, working_)
      .Entry([](int){std::cout << "starting Initializing\n";});
  working_
      .Transition(kBreak, handling_error_)
      .Transition(kNoEvent, finalizing_)
      .Entry([](int){std::cout << "starting Working\n";})
      .Do([](int){std::cout << "doing Working\n";})
      .Child(fsm_working_);
  handling_error_.Transition(kRestart, working_)
      .Transition(kContinue, working_(H))
      .Transition(kContinueDeep, working_(Hx))
      .Transition(kNext, finalizing_)
      .Transition(kBreak, machine_.Final())
      .Entry([](int){std::cout << "starting HandlingError\n";})
      .Exit([](int){std::cout << "leaving HandlingError\n";});
  finalizing_
      .Transition(kNext, initializing_)
      .Entry([](int){std::cout << "starting Finalizing\n";});
  // clang-format on
}

void MainMachine::Start() { machine_.Start(0); }

void MainMachine::DumpStates() {
  machine_.Dump([](int level, const char* name) { std::cout << level << ": " << name << " - "; });
  std::cout << std::endl;
}
