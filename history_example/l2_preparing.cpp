#include "l2_preparing.h"

#include <iostream>

#include "events.h"

using namespace fsm;

L2Preparing::L2Preparing() {
  // clang-format off
  machine_.Initial()
      .Transition(kStartEvent, l3_p1_);
  l3_p1_
      .Transition(kNext, l3_p2_)
      .Entry([](int){std::cout << "starting P1\n";});
  l3_p2_
      .Transition(kNext, l3_p3_)
      .Entry([](int){std::cout << "starting P2\n";});
  l3_p3_
      .Transition(kNext, l3_p4_)
      .Entry([](int){std::cout << "starting P3\n";});
  l3_p4_
      .Transition(kNext, machine_.Final())
      .Entry([](int){std::cout << "starting P4\n";});
  // clang-format on
}
