#ifndef L2_PREPARING_H
#define L2_PREPARING_H

#include "fsm.h"

class L2Preparing {
  fsm::Fsm<int> machine_{"L2-Preparing"};

  fsm::State<int> l3_p1_{"L3-P1"};
  fsm::State<int> l3_p2_{"L3-P2"};
  fsm::State<int> l3_p3_{"L3-P3"};
  fsm::State<int> l3_p4_{"L3-P4"};

 public:
  L2Preparing();

  constexpr operator fsm::Fsm<int> &() { return machine_; }
};

#endif  // L2_PREPARING_H
