#ifndef EVENTS_H
#define EVENTS_H

#include "event.h"

// the events used by the state machines

constexpr fsm::FsmEvent kBreak{"Break"};

constexpr fsm::FsmEvent kContinue{"Continue"};

constexpr fsm::FsmEvent kContinueDeep{"ContinueDeep"};

constexpr fsm::FsmEvent kNext{"Next"};

constexpr fsm::FsmEvent kRestart{"Restart"};

#endif  // EVENTS_H
