#include "working.h"

#include <iostream>

#include "events.h"

using namespace fsm;

Working::Working() {
  // clang-format off
  machine_.Initial()
      .Transition(kStartEvent, l2_initializing_);
  l2_initializing_
      .Transition(kNext, l2_preparing_)
      .Entry([](int){std::cout << "starting L2 Initializing\n";});
  l2_preparing_
      .Transition(kNoEvent, l2_working_)
      .Entry([](int){std::cout << "starting L2 Preparing\n";})
      .Child(preparing_);
  l2_working_
      .Transition(kNoEvent, machine_.Final())
      .Entry([](int){std::cout << "starting L2 Working\n";})
      .Do([](int){std::cout << "doing L2 Working\n";})
      .Child(working_);
  // clang-format on
}
