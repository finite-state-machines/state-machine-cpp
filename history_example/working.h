#ifndef WORKING_H
#define WORKING_H

#include "l2_preparing.h"
#include "l2_working.h"

class Working {
  L2Preparing preparing_;

  L2Working working_;

  fsm::Fsm<int> machine_{"Working"};

  fsm::State<int> l2_initializing_{"L2-Initializing"};
  fsm::State<int> l2_preparing_{"L2-Preparing"};
  fsm::State<int> l2_working_{"L2-Working"};

 public:
  Working();

  constexpr operator fsm::Fsm<int> &() { return machine_; }
};

#endif  // WORKING_H
